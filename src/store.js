import Vue from 'vue'
import Vuex from 'vuex'

const fb = require('./firebaseConfig.js')

Vue.use(Vuex)

// handle page reload
fb.auth.onAuthStateChanged(user => {
	if (user) {
		store.commit('setCurrentUser', user)
		store.dispatch('fetchUserProfile')

		fb.usersCollection.doc(user.uid).onSnapshot(doc => {
			store.commit('setUserProfile', doc.data())
		})

		fb.paymentsCollection.orderBy('date', 'desc').onSnapshot(querySnapshot => {
			let paymentsArray = []
			querySnapshot.forEach(doc => {
				let payment = doc.data()
				paymentsArray.push(payment)
			})

			store.commit('setPayments', paymentsArray)
		})
	}
})

export const store = new Vuex.Store({
	state: {
		currentUser: null,
		userProfile: {},
		payments: []
	},
	actions: {
		updateProfile({ commit, state }, data) {
			let name = data.name

			fb.usersCollection.doc(state.currentUser.uid).update({ name }).then(user => {
				console.log('User update')
			}).catch(err => {
				console.log(err)
			})
		},
		clearData({ commit }) {
			commit('setCurrentUser', null)
			commit('setUserProfile', {})
		},
		fetchUserProfile ({ commit, state }) {
			fb.usersCollection.doc(state.currentUser.uid).get().then(res => {
				commit('setUserProfile', res.data())
			}).catch(err => {
				console.log(err)
			})
		},
		updateDate({ commit, state }, data, key) {
			let description = data.description
			fb.paymentsCollection.doc(data.id).update({ description }).then(doc => {
				console.log(doc + 'update')
			}).catch((err) => {
				console.log(err)
			})
		}
	},
	mutations: {
		setCurrentUser (state, val) {
			state.currentUser = val
		},
		setUserProfile (state, val) {
			state.userProfile = val
		},
		setPayments (state, val) {
			if (val) {
				state.payments = val
			} else {
				state.payments = []
			}
		}
	}
})
