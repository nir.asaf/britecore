import firebase from 'firebase'
import 'firebase/firestore'

// firebase init goes here
const config = {
	apiKey: 'AIzaSyBaOp0kt8op8B5P8NjM859MhjdZUyq0xSU',
	authDomain: 'britecore-13b0f.firebaseapp.com',
	databaseURL: 'https://britecore-13b0f.firebaseio.com',
	projectId: 'britecore-13b0f',
	storageBucket: 'britecore-13b0f.appspot.com',
	messagingSenderId: '182072558316'
}
firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser

// date issue fix according to firebase
const settings = {
	timestampsInSnapshots: true
}
db.settings(settings)

// firebase collections
const usersCollection = db.collection('users')
const paymentsCollection = db.collection('payments')

export {
	db,
	auth,
	currentUser,
	usersCollection,
	paymentsCollection
}
