# britecore

> A Vue.js project for britecore Engineering Application

## This is my first time working with Vue.js and I love it, lightweight, modular, and requires minimal configuration 

My approach was to try to learn as much as I could in a short time. I wanted to gain experience with Manage state so I worked with Vuex and vue-router for Authenticate routes. 
This was my first time working with Vue.js and I love it. It is lightweight, modular, and requires minimal configuration. 

How long did you spend on the test? Would you do anything differently if you had more time?
``` bash 
It took me around 12h and I did that over 3 days days as I just learn Vue.js, if I would have more time I would add more features like: 
charts, some better UI like icons, also I would add tests
```

In what ways would you adapt your component so that it could be used in many different scenarios where a data table is required?
``` bash 
I would make it more dynamic so you can pass also cell header props and 
render the table accordingly 
```

What is your favorite CSS property? Why?
``` bash 
This is such a good question, I love 'position: absolute' 
The feeling that you can do everything is very nice to me
```

What is your favorite modern Javascript feature? Why?
``` bash 
I really like ES6 as it makes everything sexier 
```

What is your favorite third-party Vue.js library? Why?
``` bash 
As a new Vue.js user I really like vuematerial for some UI elements and 
vue-beautiful-chat because I want to use it in my next project 
```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```