/**
 * @jest-environment node
 */

import Vue from 'vue'
import { mount } from '@vue/test-utils'
import Login from '@/components/Login.vue'

describe('Login.vue', () => {
	it('should redirect to dashboard page when clicking on login button', () => {
		const wrapper = mount(Login, { sync: false })
		const button = wrapper.find('button')
		button.trigger('click')
		expect(window.location.href).toBe('/dashboard')
	})
})
