// Assuming you have already done "npm install fernet" https://engineering-application.britecore.com/quiz/saasdasdlflfls
let Fernet = require('fernet')
let secret = new Fernet.Secret('TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM=')
// Oh no! The code is going over the edge! What are you going to do?
let massage = 'gAAAAABcJSK3sojXNxaS2wrFB3aS8VNZY1AMhW512Rzxp9szzCk5A1RQpVKxlKWi7kvVjPHYaHYpn-6Mv4oxwlMA_yNC_5AhoFBhAvc2OEbDd1VLesBr6y3EH-c1j_Ubiv78IJigLZeBWzw9LjifuvVS-vpR-KhOv8hYtbQCPZqRNUZ37Y8nmofKokFb4wrVNq2mrMyMR-Z8'
let token = new Fernet.Token({secret: secret, token: massage, ttl:0})
console.log(token.decode())
